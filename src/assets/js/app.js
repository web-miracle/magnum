import './bootstrap';

// import Swiper from 'swiper';
import $ from 'jquery';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';

window.addEventListener( 'load', (event) => {
	$.get('https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.2/css/swiper.min.css',
		function(data) {
			var style = document.createElement('style');
			style.innerText = data;
			document.body.appendChild(style);

			// Here swiper init
			const swiper = new Swiper('.opportunity .swiper-container', {
				slidesPerView: 3,
				spaceBetween: 20,
				loop: true,
				grabCursor: true,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				breakpoints: {
					850: {
						slidesPerView: 1,
						spaceBetween: 0
					}
				}
			});

			const swop = new Swiper('.clients .swiper-container', {
				slidesPerView: 2,
				spaceBetween: 20,
				loop: true,
				grabCursor: true,
				pagination: {
					el: '.swiper-pagination',
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				breakpoints: {
					850: {
						slidesPerView: 1,
						spaceBetween: 0
					}
				}
			});

			
			// Here swiper init
		}
	);

	const lazy = new LazyLoad({
			elements_selector: ".lazyload"
		});
});